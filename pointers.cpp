///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06b - Pointers
///
/// @file pointers.cpp
/// @version 1.0
///
/// Explores pointers
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06b - Pointers - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

// In this lab, we will use a little Hungarian Notation.
// For example, variables that start with 'p' such as 'pMyChar' means that
// it's a pointer.
// For more information, checkout:  
//    https://web.mst.edu/~cpp/common/hungarian.html
//    https://en.wikipedia.org/wiki/Hungarian_notation


// Tell a story about basic datatypes
void firstStory() {
	cout << "The first story" << endl;

	// Allocate 4 variables... one char, short, int and a long
	// The first one has been done for you, it's just commented out...
   char  myChar;
   short myShort;
   int myInt;
   long myLong;
	
	// Using cout and sizeof(), print the size of the 4 basic datatypes 
	cout << "  Sizeof a char is "  << sizeof( myChar )  << " byte"  << endl;
   cout << "  Sizeof a short is "  << sizeof( myShort )  << " byte"  << endl;
   cout << "  Sizeof a int is "  << sizeof( myInt )  << " byte"  << endl;
   cout << "  Sizeof a long is "  << sizeof( myLong )  << " byte"  << endl;

	cout << endl;
		
	// Allocate 4 pointers, a pointer to a char, short, int and a long
	char*  pmyChar;
   short* pmyShort;
   int* pmyInt;
   long* pmyLong;

	// Using cout and sizeof(), print the size of the 4 pointers
	cout << "  Sizeof a char* is "  << sizeof( pmyChar )  << " bytes" << endl;	
   cout << "  Sizeof a short* is "  << sizeof( pmyShort )  << " byte"  << endl;
   cout << "  Sizeof a int* is "  << sizeof( pmyInt )  << " byte"  << endl;
   cout << "  Sizeof a long* is "  << sizeof( pmyLong )  << " byte"  << endl;

	cout << endl;
}


// Tell a story about declaring and accessing pointers.  Also, compare the
// the contents of a pointer with the address of the variable it's pointing
// to.
void secondStory() {
	cout << "The second story" << endl;

	// Allocate 4 variables (char, short, int & long).  Initialize them to
	// 100, 200, 300 and 400.
	char  myChar  = 100;  // This is the letter 'd' in ASCII
   short myShort = 200;
   int myInt = 300;
   long myLong = 400;

	// Allocate 2 pointers (char & short) using the type* name notation:
   char*  pmyChar;
	short* pmyShort;
	// Allocate 2 pointers (int & long) using the type *name notation;
	int   *pmyInt;
   long *pmyLong;

	// Using the & (address of), set each of the pointers to point to their
	// respective variable
   	pmyChar  = &myChar;
	   pmyShort = &myShort;
      pmyInt = &myInt;
      pmyLong = &myLong;

	// Using cout and the *name notation, use the pointer to print the value
	cout << "  The value that pmyChar points to is "  << *pmyChar  << endl;
   cout << "  The value that pmyShort points to is "  << *pmyShort  << endl;
   cout << "  The value that pmyInt points to is "  << *pmyInt  << endl;
   cout << "  The value that pmyLong points to is "  << *pmyLong  << endl;

	cout << endl;
	
	// Using cout, print the address of each integer datatype on one one line
	// ...and the value of each pointer on the next.
	//
	// Note:  Casting memory addresses to void* is one way to get cout to 
	//        print addresses.
	cout << "  The address of myChar is " << (void*)&myChar << endl;
	cout << "   The value of pmyChar is " << (void*)pmyChar << endl;
   cout << "  The address of myShort is " << (void*)&myShort << endl;
   cout << "   The value of pmyShort is " << (void*)pmyShort << endl;
   cout << "  The address of myInt is " << (void*)&myInt << endl;
   cout << "   The value of pmyInt is " << (void*)pmyInt << endl;
   cout << "  The address of myLong is " << (void*)&myLong << endl;
   cout << "   The value of pmyLong is " << (void*)pmyLong << endl;


	cout << endl;
}


// INTERMISSION
//
// Create 4 pointers (the usual datatypes) in "global" space
// The 's' means static.  The 'p' means pointer.
//
// Initialize them all to NULL
static char*  spmyChar  = NULL ;
static short* spmyShort = NULL;
static int* spmyInt = NULL;
static long* spmyLong = NULL;

// Let's work with dynamic memory
//
// For each of the datatypes, do the following:
//  1.  If the static pointer == NULL, then
//          Use malloc() to allocate that data on the heap
//          ...and set it to 100, 200, 300 or 400 (like above)
//      Else 
//          (if we are here, then the pointer is valid)
//          So, add 5 to the data
//  2.  Print the value 
void thirdStory() {
	cout << "The third story" << endl;

	// Step 1
	if( spmyChar == NULL ) {
		spmyChar = (char*) malloc (sizeof( char ));
		*spmyChar = 100;  
	} else {
		*spmyChar += 5;
	}

   if( spmyShort == NULL ) {
      spmyShort = (short*) malloc (sizeof( short ));
      *spmyShort = 200;
   } else {
      *spmyShort += 5;
   }

   if( spmyInt == NULL ) {
      spmyInt = (int*) malloc (sizeof( int ));
      *spmyInt = 300;
   } else {
      *spmyInt += 5;
   }

   if( spmyLong == NULL ) {
      spmyLong = (long*) malloc (sizeof( long ));
      *spmyLong = 400;
   } else {
      *spmyLong += 5;
   }	
	
	// Step 2
	cout << "  The value pointed to by spmyChar is "  << *spmyChar  << endl;
	cout << "  The value pointed to by spmyShort is "  << *spmyShort  << endl;
   cout << "  The value pointed to by spmyInt is "  << *spmyInt  << endl;
   cout << "  The value pointed to by spmyLong is "  << *spmyLong  << endl;

	cout << endl;
}


// Let's cleanup our dynamic memory
//
// For each of the datatypes, do the following:
//  1.  If the static pointer == NULL, then
//          print "  foo is NULL"
//      Else 
//          free the memory under the pointer
//          Set the pointer to NULL
//          Print a message
void fourthStory() {
	cout << "The fourth story" << endl;

	if( spmyChar == NULL ) {
		cout << "  spmyChar is NULL" << endl;
	} else {
		free( spmyChar );  // Release the memory from the heap
		spmyChar = NULL;   // Set the pointer back to NULL
		cout << "  Released memory under spmyChar " << endl;
	}

   if( spmyShort == NULL ) {
      cout << "  spmyShort is NULL" << endl;
   } else {
      free( spmyShort );  // Release the memory from the heap
      spmyShort = NULL;   // Set the pointer back to NULL
      cout << "  Released memory under spmyShort " << endl;
   }

   if( spmyInt == NULL ) {
      cout << "  spmyInt is NULL" << endl;
   } else {
      free( spmyInt );  // Release the memory from the heap
      spmyInt = NULL;   // Set the pointer back to NULL
      cout << "  Released memory under spmyInt " << endl;
   }

   if( spmyLong == NULL ) {
      cout << "  spmyLong is NULL" << endl;
   } else {
      free( spmyLong );  // Release the memory from the heap
      spmyLong = NULL;   // Set the pointer back to NULL
      cout << "  Released memory under spmyLong " << endl;
   }


	cout << endl;
}


// Pointers to objects are just like pointers to native data types.
void fifthStory() {
	cout << "The fifth story" << endl;

// Step 1:  
// Research a class called std::stringbuf
// Add the appropriate #include at the top of the file

// Step 2:
// Instantiate an instance of std::stringbuf into a variable called myObject
	std::stringbuf myObject;
	
// Step 3:
// Create a pointer object to a std::stringbuf
   std::stringbuf* pmyObject;
// Step 4:
// Set the pointer you created in Step 3 = to the address of myObject 
	pmyObject = &myObject;
// Step 5:
// Print the size of the pointer
// Print the address stored in the pointer (see, it's just like the other pointers)

   cout << "  Sizeof a pmyObject is "  << sizeof( pmyObject )  << " bytes" << endl;
   cout << "  The address of pmyObject is " << (void*)&pmyObject << endl;

// Step 6:
// Use the sgetc() method of std::stringbuf and the *name form of the pointer
   cout << "  The result of sgetc() is " << (*pmyObject).sgetc() << endl;
	
// Step 7:
// Now, on your own use the in_avail() method of std::stringbuf (look it up online!) and
// print the result

   cout << "The result of in_avail() is " << (*pmyObject).in_avail() << endl;

	cout << endl;	
}


// For our sixth story, we will look into passing data by reference...
// Type out the two "square" functions from the slides...  Call the first one 
// square( long a ) and the second one squareRef( long* a ).

// Add the two square functions here

long square ( long a ){
   a = a * a;
   return a;
}

long squareRef( long *a) {
   *a = *a * *a;
   return *a;
}
// Done with the two square functions



void sixthStory() {
	cout << "The sixth story" << endl;
	
	long a = 4;  // This will be called by value
	long b = 4;  // This will be called by reference

   // Write cout string that prints:
   //   The square of 4 is << 16
   //   The variable a is called by value, so it's still 4
   
	cout << "The square of " << a << " is " << square(a) << endl;
   cout << "The variable a is called by value so it's still " << a << endl;

   // Write cout string that prints:
   //   The square of 4 is << 16
	//   The variable b is called by reference, so it's now 16

   cout << "The square of " << b << " is " << squareRef(&b) << endl;
   cout << "The variable a is called by reference, so it is now " << b << endl;

	cout << endl;
}


int main() {
	firstStory()  ;
	secondStory() ;
	thirdStory()  ;
	thirdStory()  ;   // Call it again
	thirdStory()  ;   // ...and one more time
	fourthStory() ;
	fourthStory() ;   // Call it one more time
	fifthStory()  ;
	sixthStory()  ;
}
